module application {
    requires javafx.controls;
    requires javafx.fxml;
    requires jimObjModelImporterJFX;


    opens application.src.main.java.application to javafx.fxml;
    opens application.src.main.java.application.controller to javafx.fxml;
    opens application.src.main.java.application.view to javafx.fxml;
    opens application.src.main.java.application.model to javafx.fxml;
    opens application.src.main.java.application.util to javafx.fxml;
    exports application.src.main.java.application;
    exports application.src.main.java.application.controller;
    exports application.src.main.java.application.view;
    exports application.src.main.java.application.util;
    exports application.src.main.java.application.model;


}